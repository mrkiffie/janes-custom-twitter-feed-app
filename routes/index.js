var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', { title: "Jane's Custom Twitter Feed App" });
});

module.exports = router;
