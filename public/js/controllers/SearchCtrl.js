angular.module('SearchCtrl', ['ngResource', 'ngSanitize']).controller('SearchController', function($scope, $resource, $timeout) {

    /**
     * init controller and set defaults
     */
    function init () {

      // set a default username value
      $scope.query = "hashtag";

      // empty tweet model
      $scope.tweetsResult = [];

      $scope.getTweets();
    }
 	  /**
     * requests and processes tweet data
     */
    function getTweets (paging) {

      var params = {
        action: 'search',
        query: $scope.query
      };

      if ($scope.maxId) {
        params.max_id = $scope.maxId;
      }

      // create Tweet data resource
      $scope.tweets = $resource('/api/:action/:query', params);

      // GET request using the resource
      $scope.tweets.query( { }, function (res) {

        if( angular.isUndefined(paging) ) {
          $scope.tweetsResult = [];
        }

        $scope.tweetsResult = $scope.tweetsResult.concat(res);

        // for paging - https://dev.twitter.com/docs/working-with-timelines
        $scope.maxId = res[res.length - 1].id;

        $scope.userInfo = $scope.tweetsResult[0].user;

      });


    }

    // $scope.tweetsResult = 'Nothing beats a pocket protector!';

    /**
     * binded to @user input form
     */
    $scope.getTweets = function () {
      $scope.maxId = undefined;
      $('.search-list').addClass('hide');
      $('.loader').removeClass('hide');
      getTweets();
    }

    /**
     * binded to 'Get More Tweets' button
     */
    $scope.getMoreTweets = function () {
      $('.userInfo').addClass('hide');
      $('.loader').removeClass('hide');
      getTweets(true);
    }

    init();

});

