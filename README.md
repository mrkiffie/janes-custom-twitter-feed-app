Jane's Custom Twitter Feed App
===================

My Sample application using Angular.js, Node.js, and Twitter API.


Installing and Running
----

- Install [Node.js](http://nodejs.org/).
- Install [Bower](http://bower.io/).

Clone the repo:
```
git clone git@bitbucket.org:janey/janes-custom-twitter-feed-app.git && cd janes-custom-twitter-feed-app
```

Create a config.js file using config.sample.js as a template. Fill in your Twitter App API Keys. You will need to [create a Twitter application](https://apps.twitter.com/).

Install node module dependencies:

```
npm install
```
Install bower module dependencies:

```
bower install
```
Run application:

```
node server.js
```

Go to [http://localhost:9090 ](http://localhost:9090) in your browser.
